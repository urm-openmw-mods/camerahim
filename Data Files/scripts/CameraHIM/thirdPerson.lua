local self = require('openmw.self')
local camera = require('openmw.camera')
local util = require('openmw.util')
local storage = require('openmw.storage')
local input = require('openmw.input')

local Module = require('scripts.CameraHIM.Module')
local LPFdt = require('scripts.CameraHIM.filters').LPFdt

local thirdPersonSettings = storage.playerSection('SettingsCameraHIMthirdPerson')
local controlsSettings = storage.playerSection('SettingsCameraHIMControls')

local smoothMovement = function(current, new, dt)
   return LPFdt(current, new, thirdPersonSettings:get('movementSmoothness'), dt)
end

local function normalizeAngles(v)
   return util.vector2(
   v.x - math.floor(v.x / (2 * math.pi)) * math.pi * 2,
   util.clamp(v.y, -math.pi / 2, math.pi / 2))

end

local offset = util.vector2(2 * math.pi, 0)
local smoothAngle = function(current, new, dt)
   if math.abs(new.x - current.x) > math.pi then
      if new.x < math.pi then
         new = new + offset
      else
         new = new - offset
      end
   end
   return LPFdt(current, new, thirdPersonSettings:get('rotationSmoothness'), dt)
end

local function getMovementControls()
   return util.vector2(self.controls.movement, self.controls.sideMovement)
end










local S = {
   lastCameraMode = nil,
   movement = util.vector2(0, 0),
   rotation = 0,
   currentAngle = util.vector2(0, 0),
   targetAngle = util.vector2(0, 0),
   lastControlSwitches = {},
}


local lower = 0.35
local upper = 0.6
local function mapMovement(x)

   if math.abs(x) < 0.025 then
      return 0
   end
   if lower < x and x < upper then
      return upper
   elseif -upper < x and x < -lower then
      return -upper
   else
      return x
   end
end
local function setMovementControls(v)
   self.controls.movement = mapMovement(v.x)
   self.controls.sideMovement = mapMovement(v.y)
end


local function on(state)
   if state then
      for k, v in pairs(state) do
         (S)[k] = v
      end
   else
      S.lastCameraMode = camera.getMode()
      S.movement = util.vector2(0, 0)
      S.rotation = 0
      S.currentAngle = util.vector2(0, 0)
      S.targetAngle = util.vector2(0, 0)
   end
end

local verticalAxis = util.vector3(0, 0, 1)
local forwardAxis = util.vector3(0, 1, 0)
local verticalOffset = verticalAxis * 100

local function update(dt)
   camera.setMode(camera.MODE.Static)

   local mouseMove = util.vector2(input.getMouseMoveX(), input.getMouseMoveY())
   local sensitivity = util.vector2(
   controlsSettings:get('cameraSensitivityX'),
   controlsSettings:get('cameraSensitivityY')) /
   256

   S.targetAngle = S.targetAngle + mouseMove:emul(sensitivity)
   S.targetAngle = util.vector2(
   util.clamp(S.targetAngle.x, S.currentAngle.x - math.pi / 2, S.currentAngle.x + math.pi / 2),
   util.clamp(S.targetAngle.y, -math.pi / 2, math.pi / 2))

   S.currentAngle = smoothAngle(S.currentAngle, normalizeAngles(S.targetAngle), dt)
   S.currentAngle = normalizeAngles(S.currentAngle)

   local cameraDir = util.transform.rotate(S.currentAngle.x, verticalAxis) *
   util.transform.rotate(S.currentAngle.y, forwardAxis) *
   util.vector3(1, 0, 0)
   local cameraPos = self.object.position + verticalOffset + cameraDir * thirdPersonSettings:get('cameraDistance')
   camera.setStaticPosition(cameraPos)

   local yaw = -(S.currentAngle.x + math.pi * 0.5)
   camera.setYaw(yaw)
   camera.setPitch(-S.currentAngle.y)

   local movementControls = getMovementControls():rotate(-self.object.rotation.y - S.currentAngle.x)
   S.movement = smoothMovement(S.movement, movementControls, dt)
   setMovementControls(S.movement)
   self.controls.yawChange = 0
   self.controls.pitchChange = 0
end

local function off()
   camera.setMode(S.lastCameraMode)
end

local module = {
   on = on,
   update = update,
   off = off,
   save = function() return S end,
}
return module
