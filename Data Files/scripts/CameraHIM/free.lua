local camera = require('openmw.camera')
local input = require('openmw.input')
local util = require('openmw.util')
local storage = require('openmw.storage')

local Module = require('scripts.CameraHIM.Module')
local LPFdt = require('scripts.CameraHIM.filters').LPFdt

local controlsSettings = storage.playerSection('SettingsCameraHIMcontrols')
local freeSettings = storage.playerSection('SettingsCameraHIMfree')

local filterRotation = function(current, new, dt)
   return LPFdt(current, new, freeSettings:get('rotationSmoothness'), dt)
end

local filterSpeed = function(current, new, dt)
   return LPFdt(current, new, freeSettings:get('speedSmoothness'), dt)
end

local filterDirection = function(current, new, dt)
   return LPFdt(current, new, freeSettings:get('directionSmoothness'), dt):normalize()
end

local function getCameraRotation()
   return util.vector2(camera.getYaw(), camera.getPitch())
end

local function setCameraRotation(v)
   camera.setYaw(v.x)
   camera.setPitch(v.y)
end

local speedMap = {
   [input.ACTION.CycleWeaponRight] = 1,
   [input.ACTION.CycleWeaponLeft] = -1,
   [input.ACTION.CycleSpellRight] = 1,
   [input.ACTION.CycleSpellLeft] = -1,
}
local function getTargetSpeed(currentSpeed)
   local speedDelta = 0
   for action, v in pairs(speedMap) do
      if input.isActionPressed(action) then
         speedDelta = speedDelta + v
      end
   end
   return math.max(0, currentSpeed + speedDelta * freeSettings:get('speedSensitivity'))
end

local directionMap = {
   [input.ACTION.MoveForward] = util.vector3(0, 1, 0),
   [input.ACTION.MoveBackward] = util.vector3(0, -1, 0),
   [input.ACTION.MoveLeft] = util.vector3(-1, 0, 0),
   [input.ACTION.MoveRight] = util.vector3(1, 0, 0),
   [input.ACTION.Jump] = util.vector3(0, 0, 1),
   [input.ACTION.Sneak] = util.vector3(0, 0, -1),
}
local function getTargetDirection(currentDirection, rotation)
   local transform = util.transform.rotateZ(rotation.x)
   local controlsDirection = util.vector3(0, 0, 0)
   for action, v in pairs(directionMap) do
      if input.isActionPressed(action) then
         controlsDirection = controlsDirection + v
      end
   end
   if controlsDirection:length() > 0 then
      return transform * controlsDirection:normalize()
   else
      if currentDirection:length2() == 0 then
         return transform * util.vector3(0, 1, 0)
      else
         return currentDirection
      end
   end
end

local CONTROL_MAP = input.CONTROL_SWITCH











local S = {
   rotation = util.vector2(0, 0),
   rotationChange = util.vector2(0, 0),
   position = util.vector3(0, 0, 0),
   direction = util.vector3(0, 0, 0),
   speed = 0,
   lastCameraMode = nil,
   lastControlSwitches = {},
}

local function on(state)
   if state then
      for k in pairs(state) do
         (S)[k] = (state)[k]
      end
   else
      S.lastCameraMode = camera.getMode()
      S.rotation = getCameraRotation()
      S.rotationChange = util.vector2(0, 0)
      S.position = camera.getPosition()
      S.direction = util.vector3(0, 0, 0)
      S.speed = freeSettings:get('initialSpeed')

      for _, v in pairs(CONTROL_MAP) do
         S.lastControlSwitches[v] = input.getControlSwitch(v)
      end
   end

   for _, v in pairs(CONTROL_MAP) do
      input.setControlSwitch(v, false)
   end
   camera.setMode(camera.MODE.Static)
end

local function update(dt)
   camera.setMode(camera.MODE.Static)
   local mouseMove = util.vector2(input.getMouseMoveX(), input.getMouseMoveY())
   local sensitivity = util.vector2(
   controlsSettings:get('cameraSensitivityX'),
   controlsSettings:get('cameraSensitivityY')) /
   256
   local newRotationChange = mouseMove:emul(sensitivity)
   S.rotationChange = filterRotation(S.rotationChange, newRotationChange, dt)
   local maxRotation = freeSettings:get('maxRotation') * 2 * math.pi
   if S.rotationChange:length() > maxRotation * dt then
      S.rotationChange = S.rotationChange:normalize() * maxRotation * dt
   end

   if math.abs(S.rotation.y) > math.pi * 0.5 and S.rotationChange.y * S.rotation.y > 0 then
      S.rotationChange = util.vector2(S.rotationChange.x, 0)
   end
   S.rotation = S.rotation + S.rotationChange
   setCameraRotation(S.rotation)

   local targetSpeed = getTargetSpeed(S.speed)
   S.speed = filterSpeed(S.speed, targetSpeed, dt)
   local targetDirection = getTargetDirection(S.direction, S.rotation)
   S.direction = filterDirection(S.direction, targetDirection, dt)
   S.position = S.position + S.direction * S.speed * dt
   camera.setStaticPosition(S.position)
end

local function off()
   camera.setMode(S.lastCameraMode)
   for _, v in pairs(CONTROL_MAP) do
      input.setControlSwitch(v, S.lastControlSwitches[v])
   end
end

local module = {
   on = on,
   update = update,
   off = off,
   save = function() return S end,
}

return module
