local core = require('openmw.core')
local input = require('openmw.input')
local async = require('openmw.async')
local ui = require('openmw.ui')
local storage = require('openmw.storage')
local I = require('openmw.interfaces')

Mode = {}













local MODES = {
   Normal = 'default',
   Free = 'free',
   FirstPerson = 'firstPerson',
   ThirdPerson = 'thirdPerson',
}

local l10n = core.l10n('CameraHIM')

I.Settings.registerPage({
   key = 'CameraHIM',
   l10n = 'CameraHIM',
   name = 'Camera Honed Imagery Memoriam',
   description = 'Smooth camera for recording showcase and gameplay videos',
})

I.Settings.registerRenderer('CameraHIM_hotkey', function(value, set)
   return {
      template = I.MWUI.templates.textEditLine,
      props = {
         text = value and input.getKeyName(value) or '',
      },
      events = {
         keyPress = async:callback(function(e)
            set(e.code)
         end),
      },
   }
end)

I.Settings.registerRenderer('CameraHIM_currentMode', function(value)
   return {
      template = I.MWUI.templates.textNormal,
      props = {
         text = l10n(value),
      },
   }
end)

local modeSettings = storage.playerSection('SettingsCameraHIMmodes')

I.Settings.registerRenderer('CameraHIM_mode', function(_, _, argument)
   return {
      template = I.MWUI.templates.box,
      content = ui.content({
         {
            template = I.MWUI.templates.padding,
            content = ui.content({
               {
                  template = I.MWUI.templates.textNormal,
                  props = {
                     text = l10n('Activate'),
                  },
               },
            }),
         },
      }),
      events = {
         mouseClick = async:callback(function()
            modeSettings:set('currentMode', argument)
         end),
      },
   }
end)

do
   local modeSetings = {
      {
         key = 'currentMode',
         default = MODES.Normal,
         renderer = 'CameraHIM_currentMode',
         name = 'Current mode',
      },
   }
   local modeOrder = { MODES.Normal, MODES.Free, MODES.FirstPerson, MODES.ThirdPerson }
   for _, mode in ipairs(modeOrder) do
      table.insert(modeSetings, {
         key = mode,
         renderer = 'CameraHIM_mode',
         name = mode,
         description = mode .. '_description',
         argument = mode,
      })
   end
   I.Settings.registerGroup({
      key = 'SettingsCameraHIMmodes',
      page = 'CameraHIM',
      order = 0,
      l10n = 'CameraHIM',
      name = 'Camera modes',
      permanentStorage = false,
      settings = modeSetings,
   })
end

I.Settings.registerGroup({
   key = 'SettingsCameraHIMcontrols',
   page = 'CameraHIM',
   order = 1,
   l10n = 'CameraHIM',
   name = 'Keybindings',
   permanentStorage = true,
   settings = {
      {
         key = 'freeHotkey',
         default = nil,
         renderer = 'CameraHIM_hotkey',
         name = 'Toggle Free camera mode',
      },
      {
         key = 'firstPersonHotkey',
         default = nil,
         renderer = 'CameraHIM_hotkey',
         name = 'Toggle First Person camera mode',
      },
      {
         key = 'cameraSensitivityX',
         default = 1.0,
         renderer = 'number',
         name = 'Horizontal sensitivity',
      },
      {
         key = 'cameraSensitivityY',
         default = 1.0,
         renderer = 'number',
         name = 'Vertical sensitivity',
      },
   },
})

I.Settings.registerGroup({
   key = 'SettingsCameraHIMfree',
   page = 'CameraHIM',
   order = 2,
   l10n = 'CameraHIM',
   name = 'Free Mode',
   permanentStorage = true,
   settings = {
      {
         key = 'rotationSmoothness',
         default = 0.7,
         renderer = 'number',
         name = 'Rotation smoothness',
      },
      {
         key = 'maxRotation',
         default = 2,
         renderer = 'number',
         name = 'Rotation limit',
         description = 'In full 360 degree rotations per second',
      },
      {
         key = 'speedSmoothness',
         default = 0.7,
         renderer = 'number',
         name = 'Speed smoothness',
      },
      {
         key = 'initialSpeed',
         default = 100,
         renderer = 'number',
         name = 'Initial speed',
      },
      {
         key = 'speedSensitivity',
         default = 50,
         renderer = 'number',
         name = 'Speed sensitivity',
      },
      {
         key = 'directionSmoothness',
         default = 0.7,
         renderer = 'number',
         name = 'Direction smoothness',
      },
   },
})

I.Settings.registerGroup({
   key = 'SettingsCameraHIMfirstPerson',
   page = 'CameraHIM',
   order = 3,
   l10n = 'CameraHIM',
   name = 'First Person Mode',
   permanentStorage = true,
   settings = {
      {
         key = 'rotationSmoothness',
         default = 0.6,
         renderer = 'number',
         name = 'Rotation smoothness',
      },
      {
         key = 'movementSmoothness',
         default = 0.6,
         renderer = 'number',
         name = 'Movement smoothness',
      },
   },
})

I.Settings.registerGroup({
   key = 'SettingsCameraHIMthirdPerson',
   page = 'CameraHIM',
   order = 3,
   l10n = 'CameraHIM',
   name = 'Third Person Mode',
   permanentStorage = true,
   settings = {
      {
         key = 'rotationSmoothness',
         default = 0.6,
         renderer = 'number',
         name = 'Rotation smoothness',
      },
      {
         key = 'movementSmoothness',
         default = 0.6,
         renderer = 'number',
         name = 'Movement smoothness',
      },
      {
         key = 'cameraDistance',
         default = 100,
         renderer = 'number',
         name = 'Camera distance',
      },
   },
})

return {
   MODES = MODES,
}
