local storage = require('openmw.storage')
local async = require('openmw.async')
local input = require('openmw.input')

local Module = require('scripts.CameraHIM.Module')
local MODES = require('scripts.CameraHIM.settings').MODES

local controlsSettings = storage.playerSection('SettingsCameraHIMcontrols')
local modeSettings = storage.playerSection('SettingsCameraHIMmodes')

local modeHotkeys = {
   freeHotkey = MODES.Free,
   firstPersonHotkey = MODES.FirstPerson,
}

local modeModules = {
   [MODES.Normal] = {
      on = function() end,
      update = function() end,
      off = function() end,
      save = function() end,
   },
   [MODES.Free] = require('scripts.CameraHIM.free'),
   [MODES.FirstPerson] = require('scripts.CameraHIM.firstPerson'),
   [MODES.ThirdPerson] = require('scripts.CameraHIM.thirdPerson'),
}

local currentMode = MODES.Normal

modeSettings:subscribe(async:callback(function(_, key)
   if key ~= 'currentMode' then return end
   local newMode = modeSettings:get('currentMode')
   if currentMode == newMode then return end
   modeModules[currentMode].off()
   currentMode = newMode
   modeModules[currentMode].on()
end))

local function toggleMode(mode)
   mode = mode == modeSettings:get('currentMode') and MODES.Normal or mode
   modeSettings:set('currentMode', mode)
end

return {
   engineHandlers = {
      onKeyPress = function(key)
         local newMode
         for setting, mode in pairs(modeHotkeys) do
            if controlsSettings:get(setting) == key.code then
               newMode = mode
               break
            end
         end
         if newMode then
            toggleMode(newMode)
         end
      end,
      onFrame = function(dt)
         if dt > 0 then
            modeModules[currentMode].update(dt)
         end
      end,
      onLoad = function(saved)
         local savedMode = modeSettings:get('currentMode')
         if saved then
            currentMode = savedMode
            modeModules[savedMode].on(saved)
         else
            modeSettings:set('currentMode', MODES.Normal)
         end
      end,
      onSave = function()
         return modeModules[currentMode].save()
      end,
   },
}
