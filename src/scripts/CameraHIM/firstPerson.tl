local self = require('openmw.self')
local camera = require('openmw.camera')
local util = require('openmw.util')
local storage = require('openmw.storage')

local Module = require('scripts.CameraHIM.Module')
local LPFdt = require('scripts.CameraHIM.filters').LPFdt

local firstPersonSettings = storage.playerSection('SettingsCameraHIMfirstPerson')

local smoothRotation = function(current: util.Vector2, new: util.Vector2, dt: number): util.Vector2
    return LPFdt(current, new, firstPersonSettings:get('rotationSmoothness') as number, dt)
end

local smoothMovement = function(current: util.Vector2, new: util.Vector2, dt: number): util.Vector2
    return LPFdt(current, new, firstPersonSettings:get('movementSmoothness') as number, dt)
end

local record State
    lastCameraMode: camera.Mode
    rotationChange: util.Vector2
    movement: util.Vector2
end

local S: State = {
    lastCameraMode = nil,
    rotationChange = util.vector2(0, 0),
    movement = util.vector2(0, 0),
}

local function getRotationControls(): util.Vector2
    return util.vector2(self.controls.yawChange, self.controls.pitchChange)
end

local function setRotationControls(v: util.Vector2)
    self.controls.yawChange = v.x
    self.controls.pitchChange = v.y
end

local function getMovementControls(): util.Vector2
    return util.vector2(self.controls.movement, self.controls.sideMovement)
end


-- fast walking is faster than slow running
local lower = 0.35
local upper = 0.6
local function mapMovement(x: number): number
    -- make it easier to stop completely
    if math.abs(x) < 0.025 then
        return 0
    end
    if lower < x and x < upper then
        return upper
    elseif -upper < x and x < -lower then
        return -upper
    else
        return x
    end
end
local function setMovementControls(v: util.Vector2)
    self.controls.movement = mapMovement(v.x)
    self.controls.sideMovement = mapMovement(v.y)
end


local function on(state: State)
    if state then
        for k in pairs(state as table) do
            (S as table)[k] = (state as table)[k]
        end
    else
        S.lastCameraMode = camera.getMode()
        S.rotationChange = util.vector2(0, 0)
    end
end

local function update(dt: number)
    camera.setMode(camera.MODE.FirstPerson)

    S.rotationChange = smoothRotation(S.rotationChange, getRotationControls(), dt)
    S.movement = smoothMovement(S.movement, getMovementControls(), dt)
    S.movement = util.vector2(
        util.clamp(S.movement.x, -1, 1),
        util.clamp(S.movement.y, -1, 1)
    )
    setRotationControls(S.rotationChange)
    setMovementControls(S.movement)
end

local function off()
    camera.setMode(S.lastCameraMode)
end

local module: Module = {
    on = on,
    update = update,
    off = off,
    save = function(): State return S end,
}
return module